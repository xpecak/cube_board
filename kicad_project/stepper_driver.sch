EESchema Schematic File Version 4
LIBS:Cube_Board-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "Cube Control Board - Stepper driver"
Date "2019-12-05"
Rev "2"
Comp "FI MUNI"
Comment1 "Oldřich Pecák"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J11
U 1 1 5DE72770
P 7025 3000
AR Path="/5DE716BF/5DE72770" Ref="J11"  Part="1" 
AR Path="/5DEBACC1/5DE72770" Ref="J?"  Part="1" 
AR Path="/5DEBDE7B/5DE72770" Ref="J12"  Part="1" 
AR Path="/5DEBE719/5DE72770" Ref="J13"  Part="1" 
AR Path="/5DEBE721/5DE72770" Ref="J14"  Part="1" 
F 0 "J14" H 7105 2992 50  0000 L CNN
F 1 "Motor" H 7105 2901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7025 3000 50  0001 C CNN
F 3 "~" H 7025 3000 50  0001 C CNN
	1    7025 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 5DE72E70
P 4700 4075
AR Path="/5DE716BF/5DE72E70" Ref="#PWR0126"  Part="1" 
AR Path="/5DEBACC1/5DE72E70" Ref="#PWR?"  Part="1" 
AR Path="/5DEBDE7B/5DE72E70" Ref="#PWR0132"  Part="1" 
AR Path="/5DEBE719/5DE72E70" Ref="#PWR0138"  Part="1" 
AR Path="/5DEBE721/5DE72E70" Ref="#PWR0144"  Part="1" 
F 0 "#PWR0144" H 4700 3825 50  0001 C CNN
F 1 "GND" H 4705 3902 50  0000 C CNN
F 2 "" H 4700 4075 50  0001 C CNN
F 3 "" H 4700 4075 50  0001 C CNN
	1    4700 4075
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4700 4075 4700 4000
Wire Wire Line
	4700 3800 4775 3800
Wire Wire Line
	4775 3900 4700 3900
Connection ~ 4700 3900
Wire Wire Line
	4700 3900 4700 3800
Wire Wire Line
	4775 4000 4700 4000
Connection ~ 4700 4000
Wire Wire Line
	4700 4000 4700 3900
$Comp
L Device:C_Small C6
U 1 1 5DE74DBA
P 6400 2525
AR Path="/5DE716BF/5DE74DBA" Ref="C6"  Part="1" 
AR Path="/5DEBACC1/5DE74DBA" Ref="C?"  Part="1" 
AR Path="/5DEBDE7B/5DE74DBA" Ref="C10"  Part="1" 
AR Path="/5DEBE719/5DE74DBA" Ref="C14"  Part="1" 
AR Path="/5DEBE721/5DE74DBA" Ref="C18"  Part="1" 
F 0 "C18" H 6492 2571 50  0000 L CNN
F 1 "100n" H 6492 2480 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6400 2525 50  0001 C CNN
F 3 "~" H 6400 2525 50  0001 C CNN
	1    6400 2525
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C7
U 1 1 5DE75456
P 6775 2525
AR Path="/5DE716BF/5DE75456" Ref="C7"  Part="1" 
AR Path="/5DEBACC1/5DE75456" Ref="C?"  Part="1" 
AR Path="/5DEBDE7B/5DE75456" Ref="C11"  Part="1" 
AR Path="/5DEBE719/5DE75456" Ref="C15"  Part="1" 
AR Path="/5DEBE721/5DE75456" Ref="C19"  Part="1" 
F 0 "C19" H 6863 2571 50  0000 L CNN
F 1 "100u" H 6863 2480 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 6775 2525 50  0001 C CNN
F 3 "~" H 6775 2525 50  0001 C CNN
	1    6775 2525
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5DE78384
P 6350 3975
AR Path="/5DE716BF/5DE78384" Ref="R7"  Part="1" 
AR Path="/5DEBACC1/5DE78384" Ref="R?"  Part="1" 
AR Path="/5DEBDE7B/5DE78384" Ref="R12"  Part="1" 
AR Path="/5DEBE719/5DE78384" Ref="R17"  Part="1" 
AR Path="/5DEBE721/5DE78384" Ref="R22"  Part="1" 
F 0 "R22" H 6409 4021 50  0000 L CNN
F 1 "200m" H 6409 3930 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 6350 3975 50  0001 C CNN
F 3 "~" H 6350 3975 50  0001 C CNN
	1    6350 3975
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R8
U 1 1 5DE78810
P 6725 3975
AR Path="/5DE716BF/5DE78810" Ref="R8"  Part="1" 
AR Path="/5DEBACC1/5DE78810" Ref="R?"  Part="1" 
AR Path="/5DEBDE7B/5DE78810" Ref="R13"  Part="1" 
AR Path="/5DEBE719/5DE78810" Ref="R18"  Part="1" 
AR Path="/5DEBE721/5DE78810" Ref="R23"  Part="1" 
F 0 "R23" H 6784 4021 50  0000 L CNN
F 1 "200m" H 6784 3930 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" H 6725 3975 50  0001 C CNN
F 3 "~" H 6725 3975 50  0001 C CNN
	1    6725 3975
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 5DE7B23D
P 6350 4075
AR Path="/5DE716BF/5DE7B23D" Ref="#PWR0127"  Part="1" 
AR Path="/5DEBACC1/5DE7B23D" Ref="#PWR?"  Part="1" 
AR Path="/5DEBDE7B/5DE7B23D" Ref="#PWR0133"  Part="1" 
AR Path="/5DEBE719/5DE7B23D" Ref="#PWR0139"  Part="1" 
AR Path="/5DEBE721/5DE7B23D" Ref="#PWR0145"  Part="1" 
F 0 "#PWR0145" H 6350 3825 50  0001 C CNN
F 1 "GND" H 6355 3902 50  0000 C CNN
F 2 "" H 6350 4075 50  0001 C CNN
F 3 "" H 6350 4075 50  0001 C CNN
	1    6350 4075
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 5DE7B3E5
P 6725 4075
AR Path="/5DE716BF/5DE7B3E5" Ref="#PWR0128"  Part="1" 
AR Path="/5DEBACC1/5DE7B3E5" Ref="#PWR?"  Part="1" 
AR Path="/5DEBDE7B/5DE7B3E5" Ref="#PWR0134"  Part="1" 
AR Path="/5DEBE719/5DE7B3E5" Ref="#PWR0140"  Part="1" 
AR Path="/5DEBE721/5DE7B3E5" Ref="#PWR0146"  Part="1" 
F 0 "#PWR0146" H 6725 3825 50  0001 C CNN
F 1 "GND" H 6730 3902 50  0000 C CNN
F 2 "" H 6725 4075 50  0001 C CNN
F 3 "" H 6725 4075 50  0001 C CNN
	1    6725 4075
	1    0    0    -1  
$EndComp
Wire Wire Line
	6725 3875 6725 3600
Wire Wire Line
	6050 3700 6350 3700
Wire Wire Line
	6350 3700 6350 3875
Wire Wire Line
	6175 2325 6400 2325
Wire Wire Line
	6400 2425 6400 2325
Connection ~ 6400 2325
Wire Wire Line
	6400 2325 6775 2325
$Comp
L power:GND #PWR0129
U 1 1 5DE85081
P 6775 2625
AR Path="/5DE716BF/5DE85081" Ref="#PWR0129"  Part="1" 
AR Path="/5DEBACC1/5DE85081" Ref="#PWR?"  Part="1" 
AR Path="/5DEBDE7B/5DE85081" Ref="#PWR0135"  Part="1" 
AR Path="/5DEBE719/5DE85081" Ref="#PWR0141"  Part="1" 
AR Path="/5DEBE721/5DE85081" Ref="#PWR0147"  Part="1" 
F 0 "#PWR0147" H 6775 2375 50  0001 C CNN
F 1 "GND" H 6780 2452 50  0000 C CNN
F 2 "" H 6775 2625 50  0001 C CNN
F 3 "" H 6775 2625 50  0001 C CNN
	1    6775 2625
	1    0    0    -1  
$EndComp
Wire Wire Line
	6775 2425 6775 2325
Connection ~ 6775 2325
Wire Wire Line
	6775 2325 7100 2325
Text HLabel 7100 2325 2    50   Input ~ 0
V_MOT
Text HLabel 4525 3300 0    50   Input ~ 0
STEP
Text HLabel 4525 3400 0    50   Input ~ 0
DIR
Wire Wire Line
	4525 3400 4775 3400
Wire Wire Line
	4775 3300 4525 3300
$Comp
L Device:Jumper_NO_Small JP3
U 1 1 5DE8D999
P 4550 3100
AR Path="/5DE716BF/5DE8D999" Ref="JP3"  Part="1" 
AR Path="/5DEBACC1/5DE8D999" Ref="JP?"  Part="1" 
AR Path="/5DEBDE7B/5DE8D999" Ref="JP6"  Part="1" 
AR Path="/5DEBE719/5DE8D999" Ref="JP9"  Part="1" 
AR Path="/5DEBE721/5DE8D999" Ref="JP12"  Part="1" 
F 0 "JP12" H 4675 3050 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4550 3194 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4550 3100 50  0001 C CNN
F 3 "~" H 4550 3100 50  0001 C CNN
	1    4550 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP2
U 1 1 5DE8E2C0
P 4550 3000
AR Path="/5DE716BF/5DE8E2C0" Ref="JP2"  Part="1" 
AR Path="/5DEBACC1/5DE8E2C0" Ref="JP?"  Part="1" 
AR Path="/5DEBDE7B/5DE8E2C0" Ref="JP5"  Part="1" 
AR Path="/5DEBE719/5DE8E2C0" Ref="JP8"  Part="1" 
AR Path="/5DEBE721/5DE8E2C0" Ref="JP11"  Part="1" 
F 0 "JP11" H 4675 2950 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4550 3094 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4550 3000 50  0001 C CNN
F 3 "~" H 4550 3000 50  0001 C CNN
	1    4550 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP1
U 1 1 5DE8E4F5
P 4550 2900
AR Path="/5DE716BF/5DE8E4F5" Ref="JP1"  Part="1" 
AR Path="/5DEBACC1/5DE8E4F5" Ref="JP?"  Part="1" 
AR Path="/5DEBDE7B/5DE8E4F5" Ref="JP4"  Part="1" 
AR Path="/5DEBE719/5DE8E4F5" Ref="JP7"  Part="1" 
AR Path="/5DEBE721/5DE8E4F5" Ref="JP10"  Part="1" 
F 0 "JP10" H 4675 2850 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4550 2994 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4550 2900 50  0001 C CNN
F 3 "~" H 4550 2900 50  0001 C CNN
	1    4550 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2900 4775 2900
Wire Wire Line
	4775 3000 4650 3000
Wire Wire Line
	4650 3100 4775 3100
Text HLabel 3350 3725 0    50   Input ~ 0
FAULT
Wire Wire Line
	4400 3100 4450 3100
Wire Wire Line
	4450 3000 4400 3000
Wire Wire Line
	4400 2900 4450 2900
Text HLabel 3350 3850 0    50   Input ~ 0
ENABLE
Text HLabel 3350 3400 0    50   Input ~ 0
RESET
$Comp
L Device:R_Small R5
U 1 1 5DEA7751
P 3600 3600
AR Path="/5DE716BF/5DEA7751" Ref="R5"  Part="1" 
AR Path="/5DEBACC1/5DEA7751" Ref="R?"  Part="1" 
AR Path="/5DEBDE7B/5DEA7751" Ref="R10"  Part="1" 
AR Path="/5DEBE719/5DEA7751" Ref="R15"  Part="1" 
AR Path="/5DEBE721/5DEA7751" Ref="R20"  Part="1" 
F 0 "R20" H 3659 3646 50  0000 L CNN
F 1 "4K7" H 3659 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3600 3600 50  0001 C CNN
F 3 "~" H 3600 3600 50  0001 C CNN
	1    3600 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 3500 3600 3475
Wire Wire Line
	3350 3725 3600 3725
Wire Wire Line
	3600 3700 3600 3725
Connection ~ 3600 3725
$Comp
L Cube_Board-rescue:R_POT_Small-Device RV1
U 1 1 5DEAD530
P 3750 2500
AR Path="/5DE716BF/5DEAD530" Ref="RV1"  Part="1" 
AR Path="/5DEBACC1/5DEAD530" Ref="RV?"  Part="1" 
AR Path="/5DEBDE7B/5DEAD530" Ref="RV2"  Part="1" 
AR Path="/5DEBE719/5DEAD530" Ref="RV3"  Part="1" 
AR Path="/5DEBE721/5DEAD530" Ref="RV4"  Part="1" 
AR Path="/5DEAD530" Ref="RV4"  Part="1" 
F 0 "RV4" H 3691 2546 50  0000 R CNN
F 1 "R_POT_Small" H 3691 2455 50  0000 R CNN
F 2 "Potentiometer_SMD:Potentiometer_Vishay_TS53YL_Vertical" H 3750 2500 50  0001 C CNN
F 3 "~" H 3750 2500 50  0001 C CNN
	1    3750 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0131
U 1 1 5DEAE1EC
P 3750 2600
AR Path="/5DE716BF/5DEAE1EC" Ref="#PWR0131"  Part="1" 
AR Path="/5DEBACC1/5DEAE1EC" Ref="#PWR?"  Part="1" 
AR Path="/5DEBDE7B/5DEAE1EC" Ref="#PWR0137"  Part="1" 
AR Path="/5DEBE719/5DEAE1EC" Ref="#PWR0143"  Part="1" 
AR Path="/5DEBE721/5DEAE1EC" Ref="#PWR0149"  Part="1" 
F 0 "#PWR0149" H 3750 2350 50  0001 C CNN
F 1 "GND" H 3755 2427 50  0000 C CNN
F 2 "" H 3750 2600 50  0001 C CNN
F 3 "" H 3750 2600 50  0001 C CNN
	1    3750 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2350 3750 2400
Wire Wire Line
	3350 3400 3575 3400
$Comp
L Device:R_Small R4
U 1 1 5DF4E426
P 3575 3275
AR Path="/5DE716BF/5DF4E426" Ref="R4"  Part="1" 
AR Path="/5DEBACC1/5DF4E426" Ref="R?"  Part="1" 
AR Path="/5DEBDE7B/5DF4E426" Ref="R9"  Part="1" 
AR Path="/5DEBE719/5DF4E426" Ref="R14"  Part="1" 
AR Path="/5DEBE721/5DF4E426" Ref="R19"  Part="1" 
F 0 "R19" H 3634 3321 50  0000 L CNN
F 1 "4K7" H 3634 3230 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3575 3275 50  0001 C CNN
F 3 "~" H 3575 3275 50  0001 C CNN
	1    3575 3275
	1    0    0    -1  
$EndComp
Wire Wire Line
	3575 3125 3575 3175
Wire Wire Line
	3575 3375 3575 3400
$Comp
L LoaDy:STSPIN820 U2
U 1 1 5DEB4C66
P 5375 3200
AR Path="/5DE716BF/5DEB4C66" Ref="U2"  Part="1" 
AR Path="/5DEBDE7B/5DEB4C66" Ref="U3"  Part="1" 
AR Path="/5DEBE719/5DEB4C66" Ref="U4"  Part="1" 
AR Path="/5DEBE721/5DEB4C66" Ref="U5"  Part="1" 
F 0 "U5" H 5375 4165 50  0000 C CNN
F 1 "STSPIN820" H 5375 4074 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-24-1EP_4x4mm_P0.5mm_EP2.65x2.65mm" H 4925 3500 50  0001 C CNN
F 3 "" H 4925 3500 50  0001 C CNN
	1    5375 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0148
U 1 1 5DECB87E
P 6400 2625
AR Path="/5DE716BF/5DECB87E" Ref="#PWR0148"  Part="1" 
AR Path="/5DEBACC1/5DECB87E" Ref="#PWR?"  Part="1" 
AR Path="/5DEBDE7B/5DECB87E" Ref="#PWR0151"  Part="1" 
AR Path="/5DEBE719/5DECB87E" Ref="#PWR0153"  Part="1" 
AR Path="/5DEBE721/5DECB87E" Ref="#PWR0155"  Part="1" 
F 0 "#PWR0155" H 6400 2375 50  0001 C CNN
F 1 "GND" H 6405 2452 50  0000 C CNN
F 2 "" H 6400 2625 50  0001 C CNN
F 3 "" H 6400 2625 50  0001 C CNN
	1    6400 2625
	1    0    0    -1  
$EndComp
Wire Wire Line
	6175 2600 5975 2600
Wire Wire Line
	6175 2325 6175 2600
Wire Wire Line
	5975 2700 6175 2700
Wire Wire Line
	6175 2700 6175 2600
Connection ~ 6175 2600
Wire Wire Line
	5975 2900 6825 2900
Wire Wire Line
	5975 3000 6825 3000
Wire Wire Line
	5975 3100 6825 3100
Wire Wire Line
	5975 3200 6825 3200
Wire Wire Line
	5975 3700 6050 3700
Wire Wire Line
	6050 3700 6050 3800
Wire Wire Line
	6050 3900 5975 3900
Wire Wire Line
	5975 3800 6050 3800
Connection ~ 6050 3800
Wire Wire Line
	6050 3800 6050 3900
Connection ~ 6050 3700
Wire Wire Line
	5975 3600 6050 3600
Wire Wire Line
	6050 3600 6050 3500
Wire Wire Line
	6050 3400 5975 3400
Connection ~ 6050 3600
Wire Wire Line
	6050 3600 6725 3600
Wire Wire Line
	5975 3500 6050 3500
Connection ~ 6050 3500
Wire Wire Line
	6050 3500 6050 3400
Text HLabel 3725 2100 0    50   Input ~ 0
VDD
Text HLabel 3575 3125 0    50   Input ~ 0
VDD
Text HLabel 3600 3475 0    50   Input ~ 0
VDD
Text HLabel 4325 2900 0    50   Input ~ 0
VDD
Wire Wire Line
	4400 2900 4400 3000
Wire Wire Line
	4400 3000 4400 3100
Connection ~ 4400 3000
Wire Wire Line
	4325 2900 4400 2900
Connection ~ 4400 2900
Wire Wire Line
	3600 3850 3600 3725
Wire Wire Line
	3350 3850 3600 3850
Wire Wire Line
	4775 3600 4400 3600
Wire Wire Line
	4400 3600 4400 3725
Wire Wire Line
	3600 3725 4400 3725
Wire Wire Line
	4775 3500 3975 3500
Wire Wire Line
	3975 3500 3975 3400
Wire Wire Line
	3975 3400 3575 3400
Connection ~ 3575 3400
$Comp
L power:GND #PWR0150
U 1 1 5DF19B76
P 4000 2725
AR Path="/5DE716BF/5DF19B76" Ref="#PWR0150"  Part="1" 
AR Path="/5DEBACC1/5DF19B76" Ref="#PWR?"  Part="1" 
AR Path="/5DEBDE7B/5DF19B76" Ref="#PWR0152"  Part="1" 
AR Path="/5DEBE719/5DF19B76" Ref="#PWR0154"  Part="1" 
AR Path="/5DEBE721/5DF19B76" Ref="#PWR0156"  Part="1" 
F 0 "#PWR0156" H 4000 2475 50  0001 C CNN
F 1 "GND" H 4005 2552 50  0000 C CNN
F 2 "" H 4000 2725 50  0001 C CNN
F 3 "" H 4000 2725 50  0001 C CNN
	1    4000 2725
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5DF19D8F
P 4400 2600
AR Path="/5DE716BF/5DF19D8F" Ref="R6"  Part="1" 
AR Path="/5DEBACC1/5DF19D8F" Ref="R?"  Part="1" 
AR Path="/5DEBDE7B/5DF19D8F" Ref="R16"  Part="1" 
AR Path="/5DEBE719/5DF19D8F" Ref="R24"  Part="1" 
AR Path="/5DEBE721/5DF19D8F" Ref="R26"  Part="1" 
F 0 "R26" V 4450 2625 50  0000 L CNN
F 1 "10K" V 4450 2450 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4400 2600 50  0001 C CNN
F 3 "~" H 4400 2600 50  0001 C CNN
	1    4400 2600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5DF2270A
P 4400 2700
AR Path="/5DE716BF/5DF2270A" Ref="R11"  Part="1" 
AR Path="/5DEBACC1/5DF2270A" Ref="R?"  Part="1" 
AR Path="/5DEBDE7B/5DF2270A" Ref="R21"  Part="1" 
AR Path="/5DEBE719/5DF2270A" Ref="R25"  Part="1" 
AR Path="/5DEBE721/5DF2270A" Ref="R27"  Part="1" 
F 0 "R27" V 4450 2725 50  0000 L CNN
F 1 "10K" V 4450 2550 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4400 2700 50  0001 C CNN
F 3 "~" H 4400 2700 50  0001 C CNN
	1    4400 2700
	0    -1   1    0   
$EndComp
Wire Wire Line
	4500 2700 4775 2700
Wire Wire Line
	4775 2600 4500 2600
Wire Wire Line
	4300 2700 4200 2700
Wire Wire Line
	4000 2700 4000 2725
Wire Wire Line
	4300 2600 4200 2600
Wire Wire Line
	4200 2600 4200 2700
Connection ~ 4200 2700
Wire Wire Line
	4200 2700 4000 2700
Wire Wire Line
	3850 2500 4775 2500
$Comp
L Device:R_Small R28
U 1 1 5DF6EB4E
P 3750 2250
AR Path="/5DE716BF/5DF6EB4E" Ref="R28"  Part="1" 
AR Path="/5DEBACC1/5DF6EB4E" Ref="R?"  Part="1" 
AR Path="/5DEBDE7B/5DF6EB4E" Ref="R29"  Part="1" 
AR Path="/5DEBE719/5DF6EB4E" Ref="R30"  Part="1" 
AR Path="/5DEBE721/5DF6EB4E" Ref="R31"  Part="1" 
F 0 "R31" H 3809 2296 50  0000 L CNN
F 1 "91K" H 3809 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3750 2250 50  0001 C CNN
F 3 "~" H 3750 2250 50  0001 C CNN
	1    3750 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2150 3750 2100
Wire Wire Line
	3750 2100 3725 2100
$EndSCHEMATC
