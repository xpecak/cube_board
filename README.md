# Cube's board
This repo contains KiCad project and associated files for Cube's Control Board

- `/kicad_project` includes the source files of KiCad project for the board
- `/bom` contains interactive bom for the PCB
- `/gerber_jlc` has **currently outdated** gerbers for manufacturing

The board is licensed under CERN OHL-2-W, more in `LICENSE.md`.

![Cube Board](board.png)

## TODO
1. integrate MCU onto the board - Rev 2
1. switch to full trinamic driver chain - Rev 3